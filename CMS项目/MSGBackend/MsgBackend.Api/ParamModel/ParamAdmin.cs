namespace MsgBackend.Api.ParamModel
{

    public class ParamAdmin
    {
        
        public string AdminName { get; set; }

        public string AdminPwd { get; set; }

        public string Remark { get; set; }
    }

}