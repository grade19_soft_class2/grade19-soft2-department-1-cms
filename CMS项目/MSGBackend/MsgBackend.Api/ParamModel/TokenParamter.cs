namespace MsgBackend.Api.ParamModel
{
    public class TokenParamter
    {
        //token密钥
        public string Secret{get;set;}
        //发行人
        public string Issuer{get;set;}
        //token作用时间
        public int AccessExpiration{get;set;}
        //设置重新登录时间
        public int RefreshExpiration {get;set;}
    }
}