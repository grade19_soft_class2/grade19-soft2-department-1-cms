namespace MsgBackend.Api.ParamModel
{

    public class ParamComment
    {


        public string CommentContent{get;set;}
        
        //所属用户Id
        public int UsersId{get;set;}

        //所属文章Id
        public int ArticlesId{get;set;}

    }

}