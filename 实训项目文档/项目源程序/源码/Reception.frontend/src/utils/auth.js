//设置常量
const TokenKey = 'accessToken',
const RefreshTokenKey = 'refreshKey',
const UserId='userId'

//保存tokenkey，refreshtokenkey
export function setToken (token, refreshKey) {
    localStorage.setItem(TokenKey, token),
    localStorage.setItem(RefreshTokenKey, refreshKey)
}

//储存tokenkey
export function getToken () {
    return localStorage.getItem(TokenKey);
}

//储存refreshtokenkey
export function getRefreshToken () {
    return localStorage.getItem(RefreshTokenKey)
}

//保存用户id
export function getUserId(id){
    localStorage.setItem(UserId,id)
}
//储存用户id
export function getId(){
    return localStorage.getItem(UserId)
}

//清除token，refreshtokenkey
export function clearToken () {
    localStorage.removeItem(TokenKey),
    localStorage.removeItem(RefreshTokenKey)
}

//验证是否登录

export function isAuth(){
    let getToken = getToken();
    if (getToken) {
        return true;
    }
    else{
        return false;
    }
}
