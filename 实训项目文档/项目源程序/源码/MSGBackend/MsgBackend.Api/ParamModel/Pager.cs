namespace MsgBackend.Api.ParamModel
{
    public class Pager
    {
        //当前页码
        public int currentPage{get;set;}
        //每页行数
        public int pageSize{get;set;}
        //多少条数据
        public int total{get;set;}
    }
}