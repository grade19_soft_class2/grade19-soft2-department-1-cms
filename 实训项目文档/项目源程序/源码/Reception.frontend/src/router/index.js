import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './routes'


Vue.use(VueRouter)
// 导航冗余的解决
const or = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
    return or.call(this, location).catch(err => err)
}


let router = new VueRouter({
    mode: 'history',
    routes
})

export default router