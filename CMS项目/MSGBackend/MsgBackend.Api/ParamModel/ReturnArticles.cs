namespace MsgBackend.Api.ParamModel
{

    public class ReturnArticles
    {
        //文章名
        public string ArticleName { get; set; }
        //文章标题
        public string ArticleTitle { get; set; }
        //文章简介
        public string ArticleAbstract{get;set;}
        //文章内容
        public string ArticleContent{get;set;}
        //文章类型
        public int ArticleTypeId{get;set;}
    }

}