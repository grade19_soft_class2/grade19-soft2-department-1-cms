namespace MsgBackend.Api.ParamModel
{

    public class ParamUser
    {
        
        public string Username { get; set; }

        public string Password { get; set; }

        public string Remark { get; set; }
    }

}