using System;
using System.Collections.Generic;

namespace MsgBackend.Api.Models
{
    //管理员表
    public class Admins:Base
    {
        public string AdminName {get;set;}

        public string AdminPwd {get;set;}

        public virtual IEnumerable<Articles> Articles{get;set;}
    }
}