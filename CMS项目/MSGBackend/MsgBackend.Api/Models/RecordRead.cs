namespace MsgBackend.Api.Models
{
    //阅读记录表
    public class RecordRead:Base
    {

        //来自阅读用户id
        public int UsersId{get;set;}

        public virtual Users Users{get;set;}

        //阅读文章Id
        public int ArticlesId{get;set;}

        public virtual Articles Articles{get;set;}
    }
}