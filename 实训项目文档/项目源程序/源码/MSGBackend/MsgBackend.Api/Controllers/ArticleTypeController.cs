using System;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MsgBackend.Api.Models;
using MsgBackend.Api.ParamModel;
using MsgBackend.Api.Repository;
using MsgBackend.Api.Utils;

namespace MsgBackend.Api.Controllers
{
    // [Authorize] //设置token认证
    [ApiController]
    [Route("[controller]")]
    public class ArticleTypeController : ControllerBase
    {
        public IRepository<ArticleType> _articleTypeRepository;

        private TokenParamter _tokenparamter;

        private readonly IConfiguration _configuration;

        public ArticleTypeController(
            IRepository<ArticleType> articleTypeRepository,
            IConfiguration configuration
        )
        {
            _configuration = configuration;
            _articleTypeRepository = articleTypeRepository;
            _tokenparamter =
                _configuration
                    .GetSection("TokenParameter")
                    .Get<TokenParamter>();
        }

        //文章类型名列表查找
        [HttpGet("select")]
        public dynamic SelectAdmin([FromQuery] SelectMsg data)
        {
            var msg = data.msg;

            //获取当前页
            var currentPage = data.currentPage;

            //获取页面数据行数
            var pageSize = data.pageSize;

            var articleTypes = _articleTypeRepository.Table;

            var articleType =
                articleTypes
                    .Where(x => x.ArticleTypeName.Contains(msg))
                    .Where(x => x.IsDeleted == false)
                    .Take(pageSize)
                    .ToList();

            return Ok(new {
                Code = 1000,
                Data = articleType,
                Count =
                    articleTypes
                        .Where(x => x.ArticleTypeName.Contains(msg))
                        .Where(x => x.IsDeleted == false)
                        .Count()
            });
        }

        //获取文章管理所需要的类型
        [HttpGet("article")]
        public ActionResult Get()
        {
            
            var articleTypes = _articleTypeRepository.Table;
            var articleType =
                articleTypes
                    .Where(x => x.IsActived == true && x.IsDeleted == false)
                    .ToList();

            return Ok(new {
                Code = 1000,
                Data = articleType,
                Msg = "获取成功"
            });
        }

        //获取所有类型（后台使用）
        [HttpGet]
        public ActionResult Get([FromQuery] Pager pager)
        {
            //获取当前页
            var currentPage = pager.currentPage;

            //获取页面数据行数
            var pageSize = pager.pageSize;
            var articleTypes = _articleTypeRepository.Table;
            var articleType =
                articleTypes
                    .Where(x => x.IsActived == true && x.IsDeleted == false)
                    .Skip((currentPage - 1) * pageSize)
                    .Take(pageSize)
                    .ToList();

            return Ok(new {
                Code = 1000,
                Data = articleType,
                Count = articleTypes.Where(x => x.IsDeleted == false).Count(),
                Msg = "获取成功"
            });
        }
        //获取所有类型（前台使用）
        [HttpGet("any")]
        public ActionResult GetAny(){
            var Type=_articleTypeRepository.Table;
            return Ok(new {
                Code = 1000,
                Data = Type,
                Msg = "获得成功"
            });
        }
        //添加类型
        [HttpPost]
        public ActionResult Post(ParamArticleType model)
        {
            var articleType =
                new ArticleType {
                    ArticleTypeName = model.ArticleTypeName,
                    IsActived = true,
                    IsDeleted = false
                };
            _articleTypeRepository.Insert (articleType);
            return Ok(new {
                Code = 1000,
                Data = articleType,
                Msg = "添加成功"
            });
        }

        //修改类型
        [HttpPut]
        public ActionResult PutType(ParamArticleType model)
        {
            var articleType = _articleTypeRepository.GetById(model.Id);
            if (articleType != null)
            {
                articleType.ArticleTypeName = model.ArticleTypeName;
                _articleTypeRepository.Update (articleType);
                return Ok(new {
                    Code = 1000,
                    Data = articleType,
                    Msg = "修改成功"
                });
            }
            return Ok(new {
                Code = 1002,
                Data = "",
                Msg = "获取失败，请检查id是否正确"
            });
        }
    }
}
