import Layout from '../components/Layout'
// import Content from '../components/Content'
let routes = [
    {
        path: '/',
        component: Layout,
        redirect: {
            path: 'xinwen'
        },
        children: [
            {
                path: 'xinwen',
                component: () => import('../components/Xinwen')
            }
            ,
            {
                path: 'article/:key',
                name: 'aaaa',
                component: () => import('../csm/NewsComments.vue')
            },
            {
                path: 'shishi/:key',
                name: 'shishi',
                component: () => import('../csm/Xinwenshishi.vue')
            },
            {
                path: 'gongsi/:key',
                name: 'gongsi',
                component: () => import('../csm/Gongsidongtai.vue')
            },
            {
                path: 'fengcai/:key',
                name: 'fengcai',
                component: () => import('../csm/Dashenfengcai.vue')
            },
            // 关于我们模块
            {
                path: 'women',
                component: () => import('../csm/Guanyuwomen.vue')
            },
            // 热点模块
            {
                // 10天热评
                path: 'tenbuss',
                component: () => import('../csm/TenBuss.vue')
            },
            {
                // 10天热点
                path: 'tenhot',
                component: () => import('../csm/TenDaysHot.vue')
            },
            {
                // 2日热点
                path: 'towhot',
                component: () => import('../csm/TowDaysHot.vue')
            },
            {
                // 更多
                path: 'more',
                component: () => import('../csm/More.vue')
            }
        ]
    },

]

export default routes