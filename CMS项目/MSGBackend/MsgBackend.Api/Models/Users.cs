using System.Collections.Generic;

namespace MsgBackend.Api.Models
{
    //用户表
    public class Users:Base
    {
        public string Username{get;set;}

        public string Password{get;set;}

        //主外键
        public virtual IEnumerable<Comments> Comments{get;set;}

        public virtual IEnumerable<UserLike> UserLikes{get;set;}

        public virtual IEnumerable<RecordRead> RecordReads{get;set;}
    }
}
