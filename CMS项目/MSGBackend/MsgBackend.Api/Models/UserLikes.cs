using System;

namespace MsgBackend.Api.Models
{
    //用户点赞表
    public class UserLike:Base
    {

        //所属用户Id
        public int UsersId{get;set;}

        public virtual Users Users{get;set;}

        //所属文章Id
        public int ArticlesId{get;set;}

        public virtual Articles Articles{get;set;}
    }
}