using System;
using System.Collections.Generic;

namespace MsgBackend.Api.Models
{
    //文章图片表
    public class ArticlesPicture : Base
    {

        //图片路径
        public string PicturedPath { get; set; }

        public virtual IEnumerable<Articles> Articles{get;set;}
    }
}