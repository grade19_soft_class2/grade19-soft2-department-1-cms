using System;
using MsgBackend.Api.Repository;
using MsgBackend.Api.Models;
using MsgBackend.Api.Data;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace MsgBackend.Api.Repository
{
    public class EfRepository<T> : IRepository<T> where T : Base
    {
        //数据库上下文
        private MsgDb _db;

        public EfRepository(MsgDb db)
        {
            _db = db;
        }
        //内部表
        private DbSet<T> _entity;


        //给内部表传值
        protected DbSet<T> Entity
        {
            get
            {
                if (_entity == null)
                {
                    _entity = _db.Set<T>();
                }
                return _entity;
            }
        }

        //用AsQueryable方法返回IQueryable接口类型的数据
        public IQueryable<T> Table
        {
            get
            {
                return Entity.AsQueryable<T>();
            }
        }


        //根据id获得指定用户
        public T GetById(int id)
        {
            var t = Entity.Where(x => x.Id == id).FirstOrDefault();
            return t;
        }
        //插入新用户
        public void Insert(T entity)
        {
            if (entity == null)
            {
                throw new NotImplementedException();
            };
            entity.CreatedTime = DateTime.Now;
            entity.UpdatedTime = DateTime.Now;
            Entity.Add(entity);
            _db.SaveChanges();

        }
        //异步添加
        public async Task InsertAsync(T entity)
        {
            if (entity == null)
            {
                throw new NotImplementedException();
            }
            entity.IsActived = true;
            entity.IsDeleted = false;
            entity.CreatedTime = DateTime.Now;
            entity.UpdatedTime = DateTime.Now;
            await Entity.AddAsync(entity);
            await _db.SaveChangesAsync();
        }
        //添加多个数据
        public void InsertBulk(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                if (entity == null)
                {
                    throw new NotImplementedException();
                }
                entity.IsActived = true;
                entity.IsDeleted = false;
                entity.CreatedTime = DateTime.Now;
                entity.UpdatedTime = DateTime.Now;
                Entity.AddRange(entity);
                _db.SaveChanges();
            }

        }
        //异步添加多个数据
        public async Task InsertBulkAsync(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                if (entity == null)
                {
                    throw new NotImplementedException();
                }
                entity.IsActived = true;
                entity.IsDeleted = false;
                entity.CreatedTime = DateTime.Now;
                entity.UpdatedTime = DateTime.Now;
                await Entity.AddRangeAsync(entity);
                await _db.SaveChangesAsync();
            }

        }
        //更新
        public void Update(T entity)
        {
            if (entity == null)
            {
                throw new NotImplementedException();
            }
            entity.UpdatedTime = DateTime.Now;
            _db.SaveChanges();

        }
        //异步更新
        public async Task UpdateAsync(T entity)
        {
            if (entity == null)
            {
                throw new NotImplementedException();
            }
            entity.UpdatedTime = DateTime.Now;
            await _db.SaveChangesAsync();

        }
        //删除
        public void Delete(T entity)
        {
            if (entity == null)
            {
                throw new System.NullReferenceException();
            }

            entity.UpdatedTime = DateTime.Now;
            _db.SaveChanges();
        }
    }
}