using System; 
using System.Collections.Generic;
namespace MsgBackend.Api.Models
{
    //文章类型表
    public class  ArticleType:Base
    {

        //文章类型名
        public string ArticleTypeName{get;set;}

        public virtual IEnumerable<Articles> Articles{get;set;}
    }
}