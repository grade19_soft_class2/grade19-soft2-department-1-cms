using System;

namespace MsgBackend.Api.Models
{
    //评论表
    public class Comments:Base
    {

        //评论内容
        public String CommentContent{get;set;}
        
        //所属用户Id
        public int UsersId{get;set;}

        public virtual Users Users{get;set;}

        //所属文章Id
        public int ArticlesId{get;set;}

        public virtual Articles Articles{get;set;}
    }
}