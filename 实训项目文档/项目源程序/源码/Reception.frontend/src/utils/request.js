//设置拦截器
import axios from 'axios'

const instance=axios.create({
    baseURL:'https://cms.zt118.top/',
    timeout:5000
})

export default instance;