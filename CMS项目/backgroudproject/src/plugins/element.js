import Vue from 'vue'
import {
  Button, Container, Header, Menu, Dropdown, DropdownItem, DropdownMenu, Form, FormItem, Input,
  Main, Aside, Message, Submenu, MenuItem, Breadcrumb, BreadcrumbItem, Card, Col, Row, Table, TableColumn,
  Switch, Dialog, Pagination, MessageBox, Upload, Select, Option, Tooltip
} from 'element-ui'

Vue.use(Button)
Vue.use(Header)
Vue.use(Main)
Vue.use(Aside)
Vue.use(Container)
Vue.use(Menu)
Vue.use(Dropdown)
Vue.use(DropdownItem)
Vue.use(DropdownMenu)
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Input)
Vue.use(Submenu)
Vue.use(MenuItem)
Vue.use(Breadcrumb)
Vue.use(BreadcrumbItem)
Vue.use(Card)
Vue.use(Col)
Vue.use(Row)
Vue.use(Table)
Vue.use(TableColumn)
Vue.use(Switch)
Vue.use(Dialog)
Vue.use(Pagination)
Vue.use(Upload)
Vue.use(Select)
Vue.use(Option)
Vue.use(Tooltip)

Vue.prototype.$confirm = MessageBox.confirm
Vue.prototype.$message = Message
