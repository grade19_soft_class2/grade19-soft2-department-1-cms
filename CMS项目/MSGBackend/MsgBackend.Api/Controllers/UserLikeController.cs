using Microsoft.AspNetCore.Mvc;
using MsgBackend.Api.Repository;
using MsgBackend.Api.Models;
using Microsoft.Extensions.Configuration;
using MsgBackend.Api.ParamModel;
using System.Linq;
using System;

namespace MsgBackend.Api.Controllers
{
    // [Authorize] //设置token认证
    [ApiController]
    [Route("[controller]")]
    public class UserLikeController : ControllerBase
    {
        public IRepository<UserLike> _userLikeRepository;
        public IRepository<Users> _usersRepository;
        public IRepository<Articles> _articlesRepository;
        private readonly IConfiguration _configuration;
        public UserLikeController(IRepository<UserLike> UserLikeController,
        IRepository<Users> usersRepository,
        IRepository<Articles> articlesrepository,
        IConfiguration configuration
        )
        {
            _configuration = configuration;
            _userLikeRepository = UserLikeController;
            _articlesRepository = articlesrepository;
            _usersRepository = usersRepository;
        }


        //添加用户点赞


        [HttpPost]
        public ActionResult AddLike(ParamUserLike model)
        {
            var selectUserLikes = _userLikeRepository.Table;
            var selectUserLike = selectUserLikes.Where(x => x.UsersId == model.UsersId && x.ArticlesId == model.ArticlesId).ToList();
            System.Console.WriteLine(model);
            if (selectUserLike.Count() > 0)
            {
                return Ok(new
                {
                    Code = 1002,
                    Data = selectUserLike,
                    Msg = "抱歉，你已点过赞"
                });
            }
            else
            {
                var userLike = new UserLike
                {
                    Users = _usersRepository.Table.Single(r => r.Id == model.UsersId),
                    Articles = _articlesRepository.Table.Single(r => r.Id == model.ArticlesId)
                };
                _userLikeRepository.Insert(userLike);

                return Ok(new
                {
                    Code = 1000,
                    Data = model,
                    Msg = "点赞成功"
                });
            }

        }
        //根据id获得某本书的点赞数
        [HttpGet("LikeCount/{id}")]
        public ActionResult LikeCount(int id)
        {
            var UsersLike = _userLikeRepository.Table;
            var UserLikeCount = UsersLike.Where(x => x.ArticlesId == id).Count();
            return Ok(new
            {
                Code = 1000,
                Data = UserLikeCount,
                Msg = "获得成功"
            });
        }
        // 获得热点榜，根据书的id来以用户点赞的数量排行
        [HttpGet("TopLikeCount")]
        public ActionResult TopLikeCount()
        {
            var UserLike = _userLikeRepository.Table.Where(x => x.IsDeleted == false);
            var UserLikeCount = (

                from t in UserLike
                group t by new { t.ArticlesId }
                into grp
     
                orderby grp.Count() descending
                select new
                {
                    grp.Key.ArticlesId,
                    click =grp.Count()
                }
            ).Take(5).ToList();


            return Ok(new
            {
                code = 1000,
                Data = UserLikeCount,
                msg = "获得成功"
            });
        }
    }
}



