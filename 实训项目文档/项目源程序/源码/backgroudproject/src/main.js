import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/element.js'
import axios from 'axios'
// 导入字体图标样式表
import './assets/fonts/iconfont.css'
// 导入全局样式表
import './assets/css/global.css'
// 配置请求的根路径
axios.defaults.baseURL = 'http://localhost:5000/'
// 配置axios请求拦截器
axios.interceptors.request.use(config => {
  // 为请求头对象添加token验证的Authorization字段
  config.headers.Authorization = 'Bearer ' + window.sessionStorage.getItem('token')
  return config
})

// 挂载到vue的原型对象上
Vue.prototype.$http = axios

Vue.filter('dateFormat', function (originVal) {
  // 格式化为时间格式
  const dt = new Date(originVal)
  // 获取年
  const y = dt.getFullYear()
  // 获取月（月份从0开始，所以需要加1）
  // padStart(2, '0') 月份为两位，不足两位时，在前面补'0'
  const m = (dt.getMonth() + 1 + '').padStart(2, '0')
  // 获取日
  const d = (dt.getDate() + '').padStart(2, '0')
  // 时
  const hh = (dt.getHours() + '').padStart(2, '0')
  // 分
  const mm = (dt.getMinutes() + '').padStart(2, '0')
  // 秒
  const ss = (dt.getSeconds() + '').padStart(2, '0')
  return `${y}-${m}-${d} ${hh}:${mm}:${ss}`
})

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
