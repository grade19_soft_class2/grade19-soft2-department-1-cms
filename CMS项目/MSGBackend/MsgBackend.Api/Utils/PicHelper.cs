// 图片处理帮助类
// files 图片数据
// picpath 图片存储位置
// webRootPath web根目录wwwroot
using System.IO;

namespace MsgBackend.Api.Utils
{
    public class PicHelper
    {
        public static string
        getFilePath(dynamic files, string picpath, string webRootPath)
        {
            string showfilePath = "";

            foreach (var formFile in files)
            {
                if (formFile.Length > 0)
                {
                    int count = formFile.FileName.Split('.').Length;
                    string fileExt = formFile.FileName.Split('.')[count - 1]; //文件扩展名
                    long fileSize = formFile.Length; //获得文件大小，以字节为单位
                    string newFileName =
                        System.Guid.NewGuid().ToString() + "." + fileExt; //随机生成新文件

                    var filePath = webRootPath + "/" + picpath;
                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory (filePath);
                    }

                    filePath = webRootPath + "/" + picpath + "/" + newFileName;
                    showfilePath = picpath + "/" + newFileName;
                    FileHelper.CreateFile (filePath);

                    using (
                        var stream = new FileStream(filePath, FileMode.Create)
                    )
                    {
                        formFile.CopyTo (stream);
                    }
                }
            }
            return showfilePath;
        }
    }
}
