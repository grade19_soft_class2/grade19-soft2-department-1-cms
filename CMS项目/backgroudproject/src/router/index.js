import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/login'
  },
  {
    path: '/login',
    component: () => import('../views/Login')
  },
  {
    path: '/home',
    component: () => import('../views/Home'),
    redirect: '/welcome',
    children: [
      {
        path: '/welcome',
        component: () => import('../views/Welcome')
      },
      {
        path: '/users',
        component: () => import('../views/user/Users')
      },
      {
        path: '/admins',
        component: () => import('../views/user/Admins')
      }
    ]
  },
  {
    path: '/home',
    component: () => import('../views/Home'),
    redirect: '/posts',
    children: [
      {
        path: '/posts',
        component: () => import('../views/article/Posts')
      },
      {
        path: '/comment',
        component: () => import('../views/article/Comment')
      },
      {
        path: '/category',
        component: () => import('../views/article/Category')
      }
    ]
  },
  {
    path: '/home',
    component: () => import('../views/Home'),
    redirect: '/qrcode',
    children: [
      {
        path: '/qrcode',
        component: () => import('../views/system/Qrcode')
      },
      {
        path: '/slideshow',
        component: () => import('../views/system/Slideshow')
      },
      {
        path: '/siteinfo',
        component: () => import('../views/system/Siteinfo')
      },
      {
        path: '/about',
        component: () => import('../views/system/About')
      }
    ]
  }
]

const router = new VueRouter({
  routes
})

// 挂载路由导航守卫
router.beforeEach((to, from, next) => {
  // to 代表将要访问的路径
  // from代表从哪个路径跳转而来
  // next是一个函数，代表放行
  // next()放行  next('/login') 强制跳转
  if (to.path === '/login') return next()
  // 获取token
  const tokenStr = window.sessionStorage.getItem('token')
  if (!tokenStr) return next('/login')
  next()
})

export default router
