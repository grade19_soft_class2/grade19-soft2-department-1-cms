using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MsgBackend.Api.Models;
using MsgBackend.Api.ParamModel;
using MsgBackend.Api.Repository;

namespace MsgBackend.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]

    public class CommentController : ControllerBase
    {
        private IRepository<Articles> _ArticlesRepository;
        private IRepository<Users> _usersRepository;
        private IRepository<Comments> _commentsRepository;
        private readonly IConfiguration _configuration;
        public CommentController(
        IConfiguration configuration,
        IRepository<Articles> articlesrepository,
        IRepository<Comments> commentsRepository,
        IRepository<Users> usersRepository)
        {
            _ArticlesRepository = articlesrepository;
            _configuration = configuration;
            _usersRepository = usersRepository;
            _commentsRepository = commentsRepository;

        }
        //添加评论
        [HttpPost, Route("AddComment")]
        public ActionResult AddComment(ParamComment model)
        {
            var Comment = new Comments
            {
                CommentContent = model.CommentContent,
                Users = _usersRepository.Table.Single(r => r.Id == model.UsersId),
                Articles = _ArticlesRepository.Table.Single(r => r.Id == model.ArticlesId),
            };
            _commentsRepository.Insert(Comment);
            return Ok(new
            {
                Code = 1000,
                Data = model,
                Msg = "添加成功"
            });
        }
        //删除评论
        [HttpDelete("DeleteComment/{id}")]
        public ActionResult DeleteArticle(int id)
        {
            var comments = _commentsRepository.GetById(id);
            if (comments != null)
            {
                comments.IsDeleted = true;
                comments.IsActived = false;
                comments.UpdatedTime = DateTime.Now;
                _commentsRepository.Delete(comments);
                return Ok(new
                {
                    Code = 1000,
                    Data = comments,
                    Msg = "删除成功"
                });
            }
            else
            {
                return Ok(new { Code = 1001, Data = " ", Msg = "删除失败" });
            }
        }
        //评论排行榜，根据评论的排行来获得
        [HttpGet("TopComment")]
        public ActionResult TopComment()
        {
            var Usercomment = _commentsRepository.Table.Where(x => x.IsDeleted == false);
            var TopCommentS = (
                from t in Usercomment
                group t by new { t.ArticlesId }
                into gro
                orderby gro.Count() descending
                select new
                {
                    gro.Key.ArticlesId,
                    click = gro.Count()
                }
            ).Take(5).ToList()
            ;
            ;
        
                return Ok(new
            {
                code = 1000,
                Data = TopCommentS,
                msg = "获得成功"
            });
        }

        //获取所有评论
        [HttpGet]
        public ActionResult GetAllComment([FromQuery] Pager pager)
        {
            //获取当前页
            var currentPage = pager.currentPage;

            //获取页面数据行数
            var pageSize = pager.pageSize;
            var comments = _commentsRepository.Table;
            var comment =
                comments
                    .Where(x => x.IsDeleted == false)
                    .Skip((currentPage - 1) * pageSize)
                    .Take(pageSize)
                    .ToList();

            if (comment.Count() == 0)
            {
                return Ok(new { Code = 1002, Data = "", Msg = "获取失败" });
            }
            return Ok(new
            {
                Code = 1000,
                Data = comment,
                Count = comments.Where(x => x.IsDeleted == false).Count(),
                Msg = "获取成功"
            });
        }

        //获取文章的所有评论
        [HttpGet("article/{id}")]
        public ActionResult GetArticleComment(int id)
        {
            var comments = _commentsRepository.Table;
            var comment =
                comments
                    .Where(x => x.IsDeleted == false)
                    .Where(x => x.ArticlesId == id);
            if (comment.Count() == 0)
            {
                return Ok(new { Code = 1002, Data = "", Msg = "获取失败" });
            }
            return Ok(new { Code = 1000, Data = comment, Msg = "获取成功" });
        }

        //根据评论关键字查询
        [HttpGet("Select")]
        public ActionResult SelectComment([FromQuery] SelectMsg data)
        {
            var msg = data.msg;

            //获取当前页
            var currentPage = data.currentPage;

            //获取页面数据行数
            var pageSize = data.pageSize;
            var comments = _commentsRepository.Table;

            var comment =
                comments
                    .Where(x => x.CommentContent.Contains(msg))
                    .Where(x => x.IsDeleted == false)
                    .Take(pageSize)
                    .ToList();

            return Ok(new
            {
                Code = 1000,
                Data = comment,
                Count =
                    comments
                        .Where(x => x.CommentContent.Contains(msg))
                        .Where(x => x.IsDeleted == false)
                        .Count()
            });
        }
        //根据书籍id获得评论数
        [HttpGet("CommentCount/{id}")]

        public ActionResult CommentCount(int id)
        {
            var Commentss = _commentsRepository.Table;
            var CommentCounts = Commentss.Where(x => x.ArticlesId == id).Count();
            return Ok(new
            {
                Code = 1000,
                Data = CommentCounts,
                Msg = "获得成功"
            });
        }

    }
}
