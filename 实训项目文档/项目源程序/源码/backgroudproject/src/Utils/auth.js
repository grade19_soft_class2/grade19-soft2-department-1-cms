// 定义token等常量名
const TokenKey = 'accessToken'
const RefreshTokenKey = 'refreshToken'

// 设置token
export function setToken (token, refreshToken) {
  localStorage.setItem(TokenKey, token)
  localStorage.setItem(RefreshTokenKey, refreshToken)
}

// 获取token
export function gettoken () {
  return localStorage.getItem(TokenKey)
}

// 获取refreshToken
export function getrefreshToken () {
  return localStorage.getItem(RefreshTokenKey)
}

// 清除token和refreshToken
export function clearToken () {
  localStorage.removeItem(TokenKey)
  localStorage.removeItem(RefreshTokenKey)
}

// 是否登录
export function isAuth () {
  const token = gettoken()
  if (token) {
    return true
  } else {
    return false
  }
}
