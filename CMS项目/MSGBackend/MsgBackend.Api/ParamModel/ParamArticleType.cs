namespace MsgBackend.Api.ParamModel
{
    public class ParamArticleType
    {
        // 文章类型Id
        public int Id { get; set; }

        // 文章类型
        public string ArticleTypeName { get; set; }
    }
}
