// import request from "../Utils/resquest"

//获取所有用户
export function getAllUser(){
    return request.get('/user')
}

//根据条件添加用户
export function saveNewUser(data){
    return request.post('/user',data)
}

//更新用户
export function updateUser(data,id){
    return request.put(`/user/${id}`,data)
}

//删除用户
export function DelectUser(id){
    return request.delete(`/user/${id}`)
}

//用户登录
export function login(data){
    return request.post(`/user/Login`,data)
}

// 上传文章图片
export function addPicture(file){
    return request.post(`http://post.zt118.top:5000/article/addpic`,file,{"Content-Type":" multipart/form-data"});
}

//删除文章图片
export function deletePicture(file){
    return request.put(`/article/deletepic`,file);
}