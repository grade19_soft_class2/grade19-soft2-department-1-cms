using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using MsgBackend.Api.ParamModel;
using MsgBackend.Api.Repository;

namespace MsgBackend.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        //跨域
        private string allowCors = "AllowCors";

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //跨域
            services
                .AddCors(Options =>
                {
                    Options
                        .AddPolicy(allowCors,
                        builder =>
                        {
                            builder
                                .AllowAnyHeader()
                                .AllowAnyMethod()
                                .AllowAnyOrigin();
                        });
                });

            
            // 注册数据库上下文到容器
            services
                .AddDbContext<MsgBackend.Api.Data.MsgDb>(options =>
                    options.UseNpgsql(Configuration.GetConnectionString("Default")));

            // 注册对数据库的基本操作服务到容器
            services.AddScoped(typeof (IRepository<>), typeof (EfRepository<>));

            services.AddControllers();

            // 注册验证器（使用何种配置来验证token）
            services
                .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(option =>
                {
                    // 是否要求使用https
                    option.RequireHttpsMetadata = false;

                    // 是否保存token
                    option.SaveToken = true;

                    // 使用配置中间件，获得token的配置
                    var tokenParameter =
                        Configuration
                            .GetSection("TokenParameter")
                            .Get<TokenParamter>();

                    // 验证器的核心属性
                    option.TokenValidationParameters =
                        new TokenValidationParameters {
                            ValidateIssuerSigningKey = true, //要不要验证生成token的密钥
                            IssuerSigningKey =
                                new SymmetricSecurityKey(Encoding
                                        .UTF8
                                        .GetBytes(tokenParameter.Secret)),
                            ValidateIssuer = true, // 要不要验证发行token的人（个人或者组织）
                            ValidIssuer = tokenParameter.Issuer,
                            ValidateAudience = false // 是否验证受众
                        };
                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseRouting();

            app.UseStaticFiles();
            // 将token的验证注册到中间件
            app.UseAuthentication();

            app.UseAuthorization();

            // 跨域
            app.UseCors (allowCors);

            app
                .UseEndpoints(endpoints =>
                {
                    endpoints.MapControllers();
                });
        }
    }
}
