namespace MsgBackend.Api.ParamModel
{

    public class ParamArticle
    {
        //文章Id
        public int Id{get;set;}
        //文章名
        public string ArticleName { get; set; }
        //文章标题
        public string ArticleTitle { get; set; }
        //文章简介
        public string ArticleAbstract{get;set;}
        //文章内容
        public string ArticleContent{get;set;}
        //文章类型id
        public int ArticleTypeId {get;set;}
       //管理员Id
        public int AdminId{get;set;}
        //文章图片Id
        public int ArticlesPictureId{get;set;}
        // 文章类型
        public bool isActived{get;set;}

        // 文章图片地址
        public string PicPath { get; set; }

    }

}