using System.Collections.Generic;

namespace MsgBackend.Api.Models
{
    //文章表
    public class Articles:Base
    {
        //文章名
        public string ArticleName {get;set;}

        //文章标题
        public string ArticleTitle {get;set;}
        //文章简介
        public string ArticleAbstract {get;set;}
        //文章内容
        public string ArticleContent{get;set;}
        //文章类型
        public int ArticleTypeId {get;set;}
        public virtual ArticleType ArticleType{get;set;}

        //管理员Id
        public int AdminsId{get;set;}
        public virtual Admins Admins{get;set;}

        //文章图片Id
        public int ArticlesPictureId{get;set;}

        public virtual ArticlesPicture ArticlesPicture{get;set;}

        //主外键
        public virtual IEnumerable<Comments> Comments{get;set;}

        public virtual IEnumerable<RecordRead> RecordReads{get;set;}

        public virtual IEnumerable<UserLike> UserLikes{get;set;}
    }
}