import request from '../utils/request'

//登录
export function getByUser (data) {
    return request.post(`user/Login`, data)
}
//修改密码
export function UpdateUser (data) {
    return request.put(`user/UserUpdate`, data)
}
//注册
export function CreateUser(data){
    return request.post(`user`,data)
}

//获得所有推荐新闻
export function GetArticles(){
    return request.get(`article/Articles`)
}
//成功点赞
export function AddLike(data){
    return request.post(`UserLike`,data)
}
//点赞数
export function LikeCount(id){
    return request.get(`UserLike/LikeCount/${id}`)
}
//根据文章id获得对应评论
export function getComment(id){
    return request.get(`https://cms.zt118.top/Comment/article/${id}`)
}

//根据文章id获得对应阅读数
export function getRead(id){
    return request.get(`https://cms.zt118.top/RecordRead/read/${id}`)
}
//根据用户id来获得对应用户信息
export function getUserOne(id){
    return request.get(`https://cms.zt118.top/user/getUserOne/${id}`)
}
//添加评论
export function AddComment(data){
    return request.post(`https://cms.zt118.top/Comment/AddComment`,data)
}
//获得评论数
export function CommentCount(id){
    return request.get(`https://cms.zt118.top/Comment/CommentCount/${id}`)
}
//获得点赞排行榜
export function TopLikeCount(){
    return request.get(`https://cms.zt118.top/UserLike/TopLikeCount`)
}
//根据书籍id获得书籍信息
export function GetArticleOne(id){
    return request.get(`https://cms.zt118.top/article/GetOne/${id}`)
}
//获得所有新闻类型
export function GetArticleType(){
    return request.get(`https://cms.zt118.top/articleType/any`)
}

//根据类型id获得对应文章
export function ArticletypeId(id){
    return request.get(`https://cms.zt118.top/article/ArticletypeId/${id}`)
}
//评论排行榜
export function TopComment(){
    return request.get(`https://cms.zt118.top/Comment/TopComment`)
}
//添加阅读量
export function postRead(data){
    return request.post('https://cms.zt118.top/RecordRead',data)
}

//获取阅读排行
export function getReadTop(){
    return request.get('https://cms.zt118.top/RecordRead/readTop')
}