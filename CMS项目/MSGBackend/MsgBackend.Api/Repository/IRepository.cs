using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
namespace MsgBackend.Api.Repository
{
    public interface IRepository<T>{
        IQueryable<T> Table{get;}
        //根据Id获取对象
        
        T GetById(int id);
        //插入新的对象
        void Insert(T entity);
        //异步插入新的对象
        Task InsertAsync(T entity);
        //批量插入多个数据
        void InsertBulk(IEnumerable<T> entities);
        //批量异步插入多个数据
        Task InsertBulkAsync(IEnumerable<T> entities);
        //根据对象更改数据
        void Update(T entity);
        //异步更新数据
        Task UpdateAsync(T entity);
        //删除数据
        void Delete(T entity);

    }
}