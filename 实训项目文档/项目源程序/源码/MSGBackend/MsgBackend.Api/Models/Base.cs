using System;

namespace MsgBackend.Api.Models
{
    public class Base
    {
        public Base(){
            CreatedTime = DateTime.Now;
            UpdatedTime = DateTime.Now;
        }
        public int Id { get; set; }

        public DateTime CreatedTime { get; set; }

        public DateTime UpdatedTime { get; set; }

        public bool IsActived { get; set; }

        public bool IsDeleted { get; set; }
        public string Remark { get; set; }
    }
}