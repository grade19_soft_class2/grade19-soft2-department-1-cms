import Vue from 'vue'
import App from './App.vue'
import router from './router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css';
// 引入fade过度动画
import 'element-ui/lib/theme-chalk/base.css';
// 引入collapse 展开折叠动画
import CollapseTransition from 'element-ui/lib/transitions/collapse-transition';

Vue.filter('dateFormat', function (originVal) {
  // 格式化为时间格式
  const dt = new Date(originVal)
  // 获取年
  const y = dt.getFullYear()
  // 获取月（月份从0开始，所以需要加1）
  // padStart(2, '0') 月份为两位，不足两位时，在前面补'0'
  const m = (dt.getMonth() + 1 + '').padStart(2, '0')
  // 获取日
  const d = (dt.getDate() + '').padStart(2, '0')
  // 时
  const hh = (dt.getHours() + '').padStart(2, '0')
  // 分
  const mm = (dt.getMinutes() + '').padStart(2, '0')
  // 秒
  const ss = (dt.getSeconds() + '').padStart(2, '0')
  return `${y}-${m}-${d} ${hh}:${mm}:${ss}`
})

Vue.config.productionTip = false
Vue.use(ElementUI)
Vue.prototype.$bus = new Vue()

Vue.component(CollapseTransition.name, CollapseTransition)
new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
