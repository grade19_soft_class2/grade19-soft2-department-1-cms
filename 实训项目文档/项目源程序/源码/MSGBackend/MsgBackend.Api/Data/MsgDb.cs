using System;
using System.Collections.Generic;
using MsgBackend.Api.Models;
using Microsoft.EntityFrameworkCore;

namespace MsgBackend.Api.Data
{
    //数据库上下文
    public class MsgDb : DbContext
    {
        public MsgDb(DbContextOptions options):base(options)
        {

        }
        //用户表
        public DbSet<Users> Users { get; set; }
        //管理员表
        public DbSet<Admins> Admins { get; set; }
        //文章表
        public DbSet<Articles> Articles { get; set; }
        //文章图片表
        public DbSet<ArticlesPicture> ArticlesPictures { get; set; }
        //文章类型表
        public DbSet<ArticleType> ArticleTypes { get; set; }
        //文章阅读表
        public DbSet<RecordRead> RecordReads { get; set; }
        //文章点赞表
        public DbSet<UserLike> UserLikes { get; set; }
        //文章评论表
        public DbSet<Comments> Comments { get; set; }



    }
}