using System;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MsgBackend.Api.Models;
using MsgBackend.Api.ParamModel;
using MsgBackend.Api.Repository;
using MsgBackend.Api.Utils;
//管理员路由
namespace MsgBackend.Api.Controllers
{
    // [Authorize] //设置token认证
    [ApiController]
    [Route("[controller]")]
    public class AdminController : ControllerBase
    {
        public IRepository<Admins> _adminRepository;

        private TokenParamter _tokenparamter;

        private readonly IConfiguration _configuration;

        public AdminController(
            IRepository<Admins> adminRepository,
            IConfiguration configuration
        )
        {
            _configuration = configuration;
            _adminRepository = adminRepository;
            _tokenparamter =
                _configuration
                    .GetSection("TokenParameter")
                    .Get<TokenParamter>();
        }

        //添加新的管理员
        [HttpPost]
        public ActionResult Post(ParamAdmin model)
        {
            var admin =
                new Admins {
                    AdminName = model.AdminName,
                    AdminPwd = model.AdminPwd,
                    Remark = model.Remark,
                    IsActived = true,
                    IsDeleted = false
                };
            _adminRepository.Insert (admin);
            return Ok(new { Code = 1000, Data = admin, Msg = "添加成功" });
        }

        //用户名查找管理员
        [HttpGet("select")]
        public dynamic SelectAdmin([FromQuery] SelectMsg data)
        {
            var msg = data.msg;

            //获取当前页
            var currentPage = data.currentPage;

            //获取页面数据行数
            var pageSize = data.pageSize;

            var admins = _adminRepository.Table;

            var admin =
                admins
                    .Where(x => x.AdminName.Contains(msg))
                    .Where(x => x.IsDeleted == false)
                    .Take(pageSize)
                    .ToList();

            return Ok(new {
                Code = 1000,
                Data = admin,
                Count =
                    admins
                        .Where(x => x.AdminName.Contains(msg))
                        .Where(x => x.IsDeleted == false)
                        .Count()
            });
        }

        //获取所有管理员
        [HttpGet]
        public ActionResult Get([FromQuery] Pager pager)
        {
            //获取当前页
            var currentPage = pager.currentPage;

            //获取页面数据行数
            var pageSize = pager.pageSize;
            var admins = _adminRepository.Table;
            var admin =
                admins
                    .Where(x => x.IsDeleted == false)
                    .Skip((currentPage - 1) * pageSize)
                    .Take(pageSize)
                    .ToList();

            return Ok(new {
                Code = 1000,
                Data = admin,
                Count = admins.Where(x => x.IsDeleted == false).Count(),
                Msg = "获取成功"
            });
        }

        //修改管理员密码
        [HttpPut("{id}")]
        public ActionResult Put(ParamAdmin model, int id)
        {
            var user = _adminRepository.GetById(id);

            if (user != null)
            {
                user.AdminPwd = model.AdminPwd;
                _adminRepository.Update (user);
                return Ok(new { Code = 1000, Data = user, Msg = "修改成功" });
            }
            return Ok(new {
                Code = 1002,
                Data = "",
                Msg = "获取失败，请检查id是否正确"
            });
        }

        //管理员登录
        [AllowAnonymous] //匿名访问
        [HttpPost, Route("adminLogin")]
        public ActionResult AdminLogin(ParamAdmin model)
        {
            var adminName = model.AdminName;
            var adminPwd = model.AdminPwd;
            var admin =
                _adminRepository
                    .Table
                    .Where(x =>
                        x.AdminName == adminName && x.AdminPwd == adminPwd)
                    .FirstOrDefault();

            if (admin != null)
            {
                var token = TokenHelper.GenerateToekn(_tokenparamter, admin);
                var refreshToken = "100000";
                return Ok(new {
                    Code = 1000,
                    Data = admin,
                    Token = token,
                    RefreshToken = refreshToken,
                    Msg = "登录成功"
                });
            }
            else
            {
                return Ok(new { Code = 1002, Data = admin, Msg = "登录失败" });
            }
        }
    }
}
